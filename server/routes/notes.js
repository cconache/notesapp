const express = require('express');
const authFilter = require('../validators/authorization.validatior');
const router = express.Router();
const notesController = require('../controllers/notes.controller.js');
const notesValidator = require('../validators/notes.validator');

router.use( authFilter );

router.put('/:id', notesValidator.checkIdParam);
router.get('/:id', notesValidator.checkIdParam);
router.delete('/:id', notesValidator.checkIdParam);

// get note
router.get('/:id', notesController.getNote);

// get all notes for the current user
router.get('/', notesController.listNotes );

// create a new note owned by current user
router.post('/new', notesValidator.createNote, notesController.createNote );

// delete a note 
router.delete('/:id', notesController.deleteNote );

// update
router.put( '/:id', notesValidator.updateNote, notesController.updateNote );

module.exports = router;
const express = require('express');
const router = express.Router();
const notesValidator = require('../validators/notes.validator');
const ownersValidator = require('../validators/owners.validator');
const authFilter = require('../validators/authorization.validatior');
const ownersController = require('../controllers/owners.controller.js');

router.use( authFilter );

router.get('/:id', notesValidator.checkIdParam );

router.get('/:id', ownersController.getOwners);
router.post('/', ownersValidator.ownerCreateCheck, ownersController.createOwner)
module.exports = router;
const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users.controller.js');
const usersValidator = require('../validators/users.validator');
const authValidator = require('../validators/authorization.validatior');

// create a new note
router.use('/new', usersValidator.newUser );
router.use('/authenticate', usersValidator.authenticateUser);

router.post('/new', usersController.createUser);
router.post('/authenticate', usersController.authenticateUser );
router.get('/', authValidator, usersController.getUser);

module.exports = router;
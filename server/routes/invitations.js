const express = require('express');
const authFilter = require('../validators/authorization.validatior');
const invitationsValidator = require('../validators/invitations.validator');
const router = express.Router();
const invitationsController = require('../controllers/invitations.controller');

router.use( authFilter );

router.get('/', invitationsController.getInvitations);

router.post('/', invitationsValidator.checkInvitationParams, 
invitationsValidator.hasInvitationOwnership,
invitationsController.addInvitation );

router.delete('/:id', invitationsController.deleteInvitation );

module.exports = router;
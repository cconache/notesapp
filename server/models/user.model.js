const mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var jwt    = require('jsonwebtoken');
var configs = require('../config');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        trim: true },
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    passwordConf: {
        type: String,
        required: true
    }
});

// before saving a user into database
userSchema.pre('save', function (next) {
    var user = this;
    bcrypt.hash(user.password, 10, function (err, hash) {
        if (err) {
            return next(err);
        }   
        user.password = hash; 
        user.passwordConf = hash;
        next();
    });
});

// authenticate user 

userSchema.statics.authenticate = function (email, password, callback) {
    
    User.findOne({ email: email }).exec(function (err, user) {
         if (err) {
          return callback(err)
        } else if (!user) {
          var err = new Error('User not found. Do you have an account?');
          err.status = 403;
          return callback(err);
        }

        bcrypt.compare(password, user.password, function (err, result) {
          if (result === true) {
            
            let token = generateAuthToken( user ); 
            return callback(null, token);

          } else {
            var err = new Error("Invalid user credentials.");
            err.status = 401;
            return callback(err);
          }
        })
      });
  }

// get user by id
userSchema.statics.getById = function( id, callback ){

    User.findOne({ _id: id}).exec()
    .then( user => { return callback(null, user); }) 
    .catch( err =>  { return callback(err); } );

}

generateAuthToken = function( user ){
    const payload = {
        email: user.email,
        uid: user._id
    };

    var token = jwt.sign(payload, configs.secret, { expiresIn: "20d" });

    return token;
}

module.exports = mongoose.model( 'User', userSchema );
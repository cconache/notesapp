const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Owner = require('./owner.model');
const Invitation = require('./invitation.model');

const noteSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    content: String,
    tags: [String]
})

noteSchema.pre('remove', function (next) {
    
    let err = new Error("Couldn't complete this note removal.");
    err.status = 403

    Owner.remove({noteId: this._id}).exec()
    .then( _ => next() )
    .catch( _ => {next(err);});

    Invitation.remove({noteId: this.id}).exec()
    .then( _ => next() )
    .catch( _ => { console.log(err); next(err); });

});

noteSchema.statics.getById = function( id, callback ){

    Note.findOne({_id: id})
    .lean()
    .exec()
    .then( note => { 
        
        Owner.find({noteId: id})
        .lean()
        .populate('userId')
        .exec( function( err, owners){
            
            if( err || !owners ) callback(err);
            note.owners = [];
            for( owner of owners ){
                delete owner.userId.password;
                delete owner.userId.passwordConf;
                note.owners.push( owner.userId);
            }

            return callback(null, note)}
        )}
    ).catch( err => { return callback(err)});

}

Note = mongoose.model('Note', noteSchema);
module.exports = Note;
mongoose = require('mongoose');
const Invitation = require('./invitation.model');

const Schema = mongoose.Schema;

const ownerSchema = new Schema({
    noteId: { type: Schema.ObjectId, ref: 'Note',},
    userId: { type: Schema.ObjectId, ref: 'User'}
});

ownerSchema.index({ userId: 1, noteId: 1}, { unique: true });

module.exports = mongoose.model( 'Owner', ownerSchema);

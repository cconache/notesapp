var mongoose = require('mongoose');
var User = require('../user.model');
var Note = require('../note.model');
var Owner = require('../owner.model');

mongoose.connect('mongodb://localhost:27017/notesDB');

// clear the database

User.remove({});
Note.remove({});
Owner.remove({});

var users = [
    {
        firstName: "Andrei",
        lastName: "Georgescu",
        email: "ageorgescu@gmail.com",
        username: "ageorgescu",
        password: "testPasswd",
        passwordConf: "testPasswd"
    },
    {
        firstName: "Alin",
        lastName: "Dumitrache",
        email: "adumitrache@yahoo.com",
        username: "adumitrache16",
        password: "testPasswd",
        passwordConf: "testPasswd"
    },
    {
        firstName: "Gicu",
        lastName: "Radu",
        email: "gradu@gmail.com",
        username: "gradu123",
        password: "testPasswd",
        passwordConf: "testPasswd"
    }
]

var storedNotes = [];

User.create( new User (users[0]) , function(err, user) {
    console.log("create first user");

    var notes = [
        {
            title: "My business logic",
            content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget dictum sem. Ut nec urna cursus, imperdiet ante quis, facilisis est. Suspendisse ac nisi facilisis, porttitor justo nec, vehicula justo. Vivamus ac dui non quam pulvinar ornare. Cras fermentum suscipit leo, in lobortis lorem venenatis suscipit. Nunc ut risus vulputate, dignissim quam vel, ultricies massa. Integer elementum consectetur malesuada. Suspendisse ornare commodo tincidunt. Sed id est neque. Nam erat ante, lobortis ac dictum vitae, aliquet ut est. Sed quam nibh, interdum ut quam sed, laoreet tincidunt eros.",
            tags: ["business","logic", "startup"]
        },
        {
            title: "Food to buy",
            content: "a list containing food",
            tags: ["food","home"]
        }
    ]

    Note.create(notes, function(err, notes){
        console.log("created notes:", notes);
        
        storedNotes.push(notes);

        var ownerships = [];
        
        for( note of notes ){
            ownerships.push({
                noteId: note._id,
                userId: user._id
            });
        }

        Owner.create( ownerships, function(err,ownerships){
            console.log(err);
            console.log("created ownerships for first user:");
            console.log(ownerships);          
        });
    
    });  
    
});

User.create(users[1], function(err, user){
    
    console.log( "created second user")

    var notes = [
        {
            title: "Voting strategies",
            content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget dictum sem. Ut nec urna cursus, imperdiet ante quis, facilisis est. Suspendisse ac nisi facilisis, porttitor justo nec, vehicula justo. Vivamus ac dui non quam pulvinar ornare. Cras fermentum suscipit leo, in lobortis lorem venenatis suscipit. Nunc ut risus vulputate, dignissim quam vel, ultricies massa. Integer elementum consectetur malesuada. Suspendisse ornare commodo tincidunt. Sed id est neque. Nam erat ante, lobortis ac dictum vitae, aliquet ut est. Sed quam nibh, interdum ut quam sed, laoreet tincidunt eros.",
            tags: ["voting","politics", "strategy"]
        }
    ]

    Note.create(notes, function(err, notes){
        console.log("created notes:", notes);
        
        storedNotes.push(notes);

        var ownerships = [];
        
        for( note of notes ){
            ownerships.push({
                noteId: note._id,
                userId: user._id
            });
        }
        
        ownerships.push({
            noteId: storedNotes[0]._id,
            userId: user._id
        })

        p2 = Owner.create( ownerships, function(err,ownerships){
            console.log("created ownerships for second user:");
            console.log(ownerships);            
        });
    
    });  

});

User.create(users[2], function(err, user){
    
    console.log( "created third user")

    var notes = [
        {
            title: "Where I want to go in teambuilding",
            content: " list with places :::. ... .. .sis est. Suspendisse ac nisi facilisis, porttitor justo nec, vehicula justo. Vivamus ac dui non quam pulvinar ornare. Cras fermentum suscipit leo, in lobortis lorem venenatis suscipit. Nunc ut risus vulputate, dignissim quam vel, ultricies massa. Integer elementum consectetur malesuada. Suspendisse ornare commodo tincidunt. Sed id est neque. Nam erat ante, lobortis ac dictum vitae, aliquet ut est. Sed quam nibh, interdum ut quam sed, laoreet tincidunt eros.",
            tags: ["places","party", "letsGo"]
        },
        {
            title: "New article for blog",
            content: "this is the revolutionary article content",
            tags: ["article", "inovation", "hot", "fresh"]
        }
    ]

    Note.create(notes, function(err, notes){
        console.log("created notes:", notes);
        
        storedNotes.push(notes);

        var ownerships = [];
        
        for( note of notes ){
            ownerships.push({
                noteId: note._id,
                userId: user._id
            });
        }
        
        ownerships.push({
            noteId: storedNotes[0]._id,
            userId: user._id
        })

        ownerships.push({
            noteId: storedNotes[1]._id,
            userId: user._id
        })

        Owner.create( ownerships, function(err,ownerships){
            console.log("created ownerships for third user:");
            console.log(ownerships);            
        });
    
    });  

});






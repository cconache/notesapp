mongoose = require('mongoose');
const Schema = mongoose.Schema;

const invitationSchema = new Schema({
    noteId: { type: Schema.ObjectId, ref: 'Note', required: true},
    userId: { type: Schema.ObjectId, ref: 'User', required: true},
    hostId: { type: Schema.ObjectId, ref: 'User', required: true}
});


module.exports = mongoose.model( 'Invitation', invitationSchema);

const express = require('express');
var app = express();    
var bodyParser = require('body-parser');
var logger = require('morgan')
var router = express.Router();
var cors = require('cors')
var socketIO = require('socket.io');
var socketHandler = require('./handlers/socket.handler');
const port = 8080;
const mongoose = require('mongoose');
const assert = require('assert');
const http = require('http');
const configs = require('./config');
const users = require('./routes/users');
const notes = require('./routes/notes');
const owners = require('./routes/owners');
const invitations = require('./routes/invitations');

mongoose.connect( configs.database.url );

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(logger('dev'));

app.use('/notes', notes);
app.use('/users', users);
app.use('/owners', owners);
app.use('/invitations', invitations);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {

    req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.send({
        message: err.message,
        error: err
    });
    return;

});

const server = http.createServer(app);

server.listen(port);
server.on('listening', () => {
    console.log( `Started listening on port ${port}`);
})

// -- sockets 
socketHandler.init(server);

// authorize every incoming socket;



// io.use( (socket, err) => {
//     console.log( err.message ); 
// })



var config = {
    database:{
        url: 'mongodb://localhost:27017/notesDB'
    },
    secret: "ThisisASecretPrivateKey"
}

module.exports = config;
const Invitation = require('../models/invitation.model');
const User = require('../models/user.model');

const socketHandler = require('../handlers/socket.handler');

getInvitations = function(req, res, next) {

    let error = new Error("Some problems were encountered while searching owners for this note");
    error.status = 403;
    
    let userId = req.decoded.uid;

    Invitation.find({userId: userId})
    .populate('hostId')
    .populate('noteId')
    .lean()
    .exec()
    .then( invitations => {

        if(!invitations) next(error);

        for( let invitation of invitations ){
            invitation["uid"] = invitation._id;
            delete invitation._id;
            delete invitation.hostId.password;
            delete invitation.hostId.passwordConf;
            delete invitation.userId
            invitation["note"] = invitation.noteId;         
            invitation["host"] = invitation.hostId.email;                      
            delete invitation.hostId
            delete invitation.noteId;            
        }

        res.json({invitations: invitations});

    })
    .catch( _ => next(error))

}

addInvitation = function( req, res, next){

    User.findOne({email: req.body.email})
    .exec()
    .then( user =>{

        if( !user ){
            let error = new Error("There's no user that coresponds to this email.");
            error.status = 400;
            next(error);
        }else{

            let hostId = req.decoded.uid;
            let userId = user._id;

            let invitation = new Invitation({
                noteId: req.body.noteId,
                userId: userId,
                hostId: hostId
                
            });

            Invitation.create( invitation, function( err, invitation){
                if( err ){
                    err = new Error("Invitation couldn't be sent.")
                    err.status = 422;
                    next(err);
                } else{
                    res.json({invitation: invitation});
        
                    let note = { noteId: req.body.noteId,
                                 title: req.body.noteTitle 
                                } 
                    
                    let invitationInfo = {
                        uid: invitation._id,
                        host: req.decoded.email,
                        note: note
                    }
        
                    socketHandler.sendInvitationRequest( invitationInfo, userId)
                }
            });
        
        }
    })
}

deleteInvitation = function(req, res, next){

    let error = new Error("Some problems were encountered while declining this invitation.");
    error.status = 403;

    let userId = req.decoded.uid;
    let invitationId = req.params.id;

    Invitation.findOne({_id: invitationId, userId: userId})
    .exec()
    .then( invitation => {

        if(!invitation){
            res.json({"message": "Invitation successfully declined."}) 
        }

        invitation.remove().then( _ => {
            res.json({"message": "Invitation successfully declined."})     
        })
        .catch( err => {
            console.log(err);
            next(error);
        });

    }).catch( _ => next(error));

}

module.exports = {
    getInvitations,
    addInvitation,
    deleteInvitation
}
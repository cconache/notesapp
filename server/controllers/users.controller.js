User = require('../models/user.model');

createUser = function( req, res, next) {

    var user = new User ({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        passwordConf: req.body.passwordConf
    });
    
    User.create( user, function( err, usr){
        if( err ){
            err = new Error("User couldn't be created. Try again with another username and email.")
            err.status = 422;
            next(err);
        
        } else{
            res.json({"message": "User successfully created."});
        }
    });
};

getUser = function ( req, res, next ){

    let error = new Error("User not found");
    error.status = 404;

    User.findOne({_id: req.decoded.uid})
    .lean()
    .exec()
    .then( user => {
        if( !user ) next(error);
        else{
            delete user.password;
            delete user.passwordConf;
            res.json(user);
        }
    })
    .catch( _ => next(error));
}

authenticateUser = function( req, res, next){
    User.authenticate( req.body.email, req.body.password, function(err, authToken){
        if( err ){
            next(err);
        }else{
            res.json({"token": authToken});
        }
    });
};

module.exports = {
    createUser,
    authenticateUser,
    getUser
}
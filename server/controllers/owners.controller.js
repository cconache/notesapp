const Owner = require('../models/owner.model');
const Note = require('../models/note.model');
const Invitation = require('../models/invitation.model');
const socketHandler = require('../handlers/socket.handler');

getOwners = function(req, res, next) {

    let error = new Error("Some problems were encountered while searching owners for this note");
    error.status = 403;
    
    let noteId = req.params.id;

    Owner.find({noteId: noteId})
    .populate("userId")
    .lean()
    .exec()
    .then( owners => {
        if( !owners ) next(error);

        var response = [];
        for( owner of owners ){
            delete owner.userId.password;
            delete owner.userId.passwordConf;
            response.push(owner.userId);
        }

        return res.json({owners: response});

    }).catch( _ => next(error) );

}

createOwner = function(req, res, next){

    let invitationId = req.body.invitationId;

    var error = new Error("Some problems were encountered while trying to accept the invitation.");
    error.status = 403;

    Invitation.findOne({_id: invitationId})
    .exec()
    .then( invitation => {

        var invalidInv = new Error("You don't have a valid invitation.")
        invalidInv.status = 401;

        if( !invitation ){
            next(invalidInv);
            
        }else if( invitation.userId != req.decoded.uid ) {
            next(invalidInv);
        } else {
        
           Note.findOne({_id: invitation.noteId}).exec()
            .then( note => {

                if( !note ) res.json({});
                else{
                    
                    invitation.remove();

                    let owner = new Owner({
                        userId: invitation.userId,
                        noteId: invitation.noteId
                    });

                    Owner.create( owner , function( err, owner){
                        if( err ){
                            
                           err = new Error("Couldn't accept invitation.");
                           err.status = 422;
                           next(err);
                        
                        } else {
                            res.json({message: "Collaboration successfully accepted."}); 
                            socketHandler.joinNoteRoom( invitation.noteId, invitation.userId);
                            socketHandler.sendRoomMessage( invitation.noteId, { type: "owner-new", target: invitation.noteId}, req.decoded.uid);
                        }
                    });
                }
            }).catch( _ => next(error));

        }

    }).catch(_ => next(error));
}

module.exports = {
    getOwners,
    createOwner
}
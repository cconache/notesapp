const Note = require('../models/note.model');
const Owner = require('../models/owner.model');
const socketHandler = require('../handlers/socket.handler');

listNotes = function( req, res, next){
    
    let error = new Error("Some problems were encountered while searching notes for this user");
    error.status = 403;
    
    Owner.find({userId: req.decoded.uid}).exec()
    .then( ownershipData => {
        
        noteIDs = ownershipData.map( data => data.noteId );

        Note.find().where('_id').in(noteIDs).exec()
        .then( notes => {
            res.json({"notes": notes})
            
            for( let note of notes){
                socketHandler.joinNoteRoom(note._id, req.decoded.uid);
            }

        })
        .catch( _ => next(error) );          
        
    }).catch( _ => next(error) );

}

getNote = function( req, res, next ){

    let id = req.params.id;
   
    Owner.find({userId: req.decoded.uid}).exec()
    .then( ownershipData => {

        userOwnsNote = ownershipData.filter( data => data.noteId == id ).length > 0;    
        
        if( !userOwnsNote ){

            let error = new Error("You don't have permissions to access this note data.");
            error.status = 401;
            next(error);
        
        } else {

            Note.getById(req.params.id, function(err, note){
                if( err ){
                    let error = new Error("Something went wrong while getting the note.");
                    error.status = 400;
                    next(error);
                } else res.json({"note": note});
            });

        }

    }).catch( _ => next(error) );

}

createNote = function( req, res, next){
    
    let note = new Note({
        title: req.body.title,
        content: req.body.content,
        tags: req.body.tags
    });

    Note.create( note, function( err, note){
       
        if( err ){
            
            err = new Error("Note couldn't be created.");
            err.status = 422;
            next(err);
        
        } else {

            let uid = req.decoded.uid;
            assignNoteOwner( note._id, uid).then( _ => {
                res.json(note);
                socketHandler.joinNoteRoom(note._id, uid);
            })            
        }
    });

}

deleteNote = function( req, res, next){

    let id = req.params.id;
    let error = new Error("Couldn't remove the note.");
    
    Note.findOne({_id: id}).exec()
    .then( note => {
        
        if( !note ){
            res.json({"message": "Note successfully deleted."});
            socketHandler.sendRoomMessage( id, { type: "note-delete", target: id}, req.decoded.uid);               
        }

        note.remove().then( _ => {
            res.json({"message": "Note successfully deleted."})
            socketHandler.sendRoomMessage( id, { type: "note-delete", target: id}, req.decoded.uid);               
        })
        .catch( err => {
            console.log(err);
            next(error);
        });

    });
}

updateNote = function( req, res, next){

    let id = req.params.id;
    let note = {}
    let hasNoteChanges = false;

    if( req.body.title != "" ){
        hasNoteChanges = true;
        note.title = req.body.title;
    }

    if( req.body.content != "" ){
        hasNoteChanges = true;
        note.content = req.body.content;
    }

    if( req.body.tags.length > 0 ){
        hasNoteChanges = true;
        note.tags = req.body.tags;
    }
    
    if( hasNoteChanges ){
        
        // update note infos

        Note.update({_id: id}, note ).exec()
        .then( note => {
            socketHandler.sendRoomMessage( id, { type: "note-update", target: id}, req.decoded.uid);            
            res.json(note);
        })
        .catch( err => {
            console.log(err);
           let error = new Error("An unexpected error occured while trying save the changes for this note. Pleas try again!");
           error.status = 403;
           next(error); 
        });


    } else res.json({});

}

assignNoteOwner = function ( noteId, uid ){

    return new Promise( (resolve, reject) => {
        
        Owner.findOne({noteId: noteId, userId: uid}).exec()
        .then( own => {
            if( own ) reject();
            else {
                ownership = new Owner({
                    noteId: noteId,
                    userId: uid
                });
    
                resolve(ownership.save());
            }        
        });

    })
}

module.exports = {
    listNotes,
    createNote,
    deleteNote,
    updateNote,
    getNote
}
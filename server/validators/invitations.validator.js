const Owner = require('../models/owner.model');

checkInvitationParams = function( req, res, next){

    const incompleteRequestErr = new Error("Incomplete request");
    incompleteRequestErr.status = 400;

    if( req.body.email && req.body.noteId ){
        next();
    }else{
        next(incompleteRequestErr);
    }

}

hasInvitationOwnership = function( req, res, next){

    const error = new Error("You don't own this note. In order to send an ownership invite you have to get ownership access for it.");
    error.status = 401;

    Owner.findOne({userId: req.decoded.uid, noteId: req.body.noteId})
    .populate("noteId")
    .exec()
    .then( ownership => {

        req.body.noteTitle = ownership.noteId.title;
        if( !ownership ) next(error);
        else next();

    })
    .catch( _ => next(error));

}

module.exports = {
    checkInvitationParams,
    hasInvitationOwnership
}
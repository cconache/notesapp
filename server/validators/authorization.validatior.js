const configs = require('../config');
var jwt = require('jsonwebtoken');

module.exports = function( req, res, next ){
    
    var token = req.headers['x-auth-token'];
    
    if( !token ) {
        err = new Error("User not authorized for this opperation. Please provide speciffic auth token.");
        err.status = 401;
        next(err);
    } else{
        // verifies secret and checks exp
        jwt.verify(token, configs.secret, function(err, decoded) {      

        if (err) {
          err = new Error("The authentication token is not valid.");
          err.status = 401;
          next(err);
        } else {
          req.decoded = decoded; 
          next();
        }
      });
    }
}

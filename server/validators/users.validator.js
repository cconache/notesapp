const incompleteRequestErr = new Error("Incomplete request");
incompleteRequestErr.status = 400;

newUser = function(req, res, next ){
    if( req.body.email &&
        req.body.firstName &&
        req.body.lastName &&
        req.body.username &&
        req.body.password &&
        req.body.passwordConf ){
            next();
        } else{
            next( incompleteRequestErr );
        }
}

authenticateUser = function( req, res, next){
    if( req.body.email &&
        req.body.password ){
            next();
        } else {
            next(incompleteRequestErr);
        }
}

module.exports = {
    newUser,
    authenticateUser
}
ownerCreateCheck = function( req, res, next){

    if( req.body.invitationId && req.body.invitationId.length > 0){
        next();
    }else{
        let error = new Error("Incomplete request");
        error.status = 400
        next(error)
    }
}

module.exports = {
    ownerCreateCheck
}
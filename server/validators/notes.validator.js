const Note = require('../models/note.model');
const incompleteRequestErr = new Error("Incomplete request");
incompleteRequestErr.status = 400;

updateNote = function(req, res, next){

    if( (!req.body.title && !req.body.content && !req.body.tags) && 
        !req.body.newOwner ) {
            next( incompleteRequestErr );
    } else{    
        req.body.title = req.body.title || "";
        req.body.content = req.body.content || "";
        req.body.tags = req.body.tags || [];
        next();
    }
};

createNote = function( req, res, next ){

    req.body.title = req.body.title || "Untitled"
    req.body.content = req.body.content || "";
    req.body.tags = req.body.tags || [];
    next();

}

checkIdParam = function( req, res, next) {
    
    Note.findById(req.params.id, function(err, note){

        if( err || note == null ){
            next('router');
        } else{
            next();
        }
        
    });
};

module.exports = {
    updateNote,
    checkIdParam,
    createNote
}
var jwt = require('jsonwebtoken');
const configs = require('../config');
var socketIO = require('socket.io');

var io = null;
var socketObjects = {}

init = function( server ){

    io = socketIO.listen(server);

    io.on('connection', (socket) => {
        io.use( authorize );  
        
        socket.on('room-message', (message) => {
            room = message.room
            console.log(message);
            console.log(`writing to ${room}`);
            // io.to(room).emit( message.type, message);
            socket.broadcast.to(room).emit(message.type, message);
        });
        
    });

        
}

authorize = function ( socket, next ){

    let authError = new Error("Not authorized.");

    if( !socket.handshake.query.token ){
        socket.emit('unauthorized');
    }

    token = socket.handshake.query.token

    jwt.verify(token, configs.secret, function(err, decoded) {                  
        if (err) {
            socket.emit('unauthorized');   
        } else {
            socketObjects[decoded.uid] = socket;
            socketObjects[decoded.uid].emit('authorized');
        }
    });
    
    next();
}

sendInvitationRequest = function( invitationInfo, receiverUid ) {

    if( !invitationInfo.host || !invitationInfo.note) return;

    console.log(receiverUid);
    if( socketObjects.hasOwnProperty(receiverUid) ){
        console.log(`writing to ${receiverUid}`);
        console.log(Object.keys(socketObjects));
        socketObjects[receiverUid].emit("invitation request", 
            invitationInfo);

    }

}

joinNoteRoom = function( noteId, receiverUid){

    if( socketObjects.hasOwnProperty(receiverUid) ){
        console.log(`${receiverUid} joined to ${noteId}`);
        socketObjects[receiverUid].join(noteId);
    }

}

sendRoomMessage = function( room, message, senderUid){
    console.log("Asdasdasdasd");
    console.log(senderUid);
    if( socketObjects.hasOwnProperty(senderUid) ){
        console.log(`writing to ${room}`);
        socketObjects[senderUid].broadcast.to(room).emit(message.type, message);
    }

}

module.exports = {
    authorize,
    sendInvitationRequest,
    init,
    joinNoteRoom,
    sendRoomMessage
}
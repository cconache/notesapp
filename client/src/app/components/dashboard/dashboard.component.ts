import { Component, OnInit } from '@angular/core';

import {NotesService} from '../../services/notes-service/notes.service';
import {InvitationsService} from '../../services/invitations-service/invitations.service';

import {Note} from '../../objects/Note';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  currentNote: Note;

  constructor( private notesService: NotesService, private invitationsService: InvitationsService) { 
    // console.log(`dashboard: ${this.socketService.getInvitations()}`);
  }

  ngOnInit() {}

  public addEmptyNote(): void {
    
    let note: JSON = <JSON> {};
    this.notesService.createNote( note );

  }

  public showEditableNote( note: Note ): void {
    this.currentNote = note;
  }

}

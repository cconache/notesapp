import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services/notification-service/notification.service';

import { Notification } from '../../objects/Notification';
import { Success } from '../../objects/Success';
import { Error } from '../../objects/Error';

@Component({
  selector: 'notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})

 export class NotificationComponent implements OnInit {

  private notifications: Notification[]= [];

  constructor( private notificationService: NotificationService ) { }

   ngOnInit() {
    
    this.notificationService.getActiveNotification().subscribe( 
      notification => {
        this.notifications.unshift(notification);
        setTimeout( this.deleteNotification.bind(this, notification) , 10000);
      }
    );
  
  }
   
  deleteNotification( notification: Notification ): void {
    
    let index = this.notifications.indexOf(notification);
    if( index >= 0 ){
      this.notifications.splice(index,1);
    }

  }

  isSuccessNotification( notification: Notification ): boolean{
    return notification instanceof Success;
  }

  isErrorNotification( notification: Notification ): boolean{
    return notification instanceof Error;
  }


 }

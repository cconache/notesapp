import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import {NotesService} from '../../services/notes-service/notes.service';
import { Observable } from 'rxjs/Observable';

import { Note } from '../../objects/Note';

@Component({
  selector: 'notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.css']
})

export class NotesListComponent implements OnInit {

  @Output()
  show: EventEmitter<Note> = new EventEmitter();

  constructor( private notesService: NotesService) { }

  ngOnInit() {
  }

  public getAllNotes(): Observable<Note[]> {
    return this.notesService.getLoadedNotes();
  }

  public emitShowEvent( note: Note ): void {
    this.show.emit(note);
  }
  
}

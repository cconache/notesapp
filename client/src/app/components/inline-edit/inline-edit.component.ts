import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'inline-edit',
  templateUrl: './inline-edit.component.html',
  styleUrls: ['./inline-edit.component.css']
})

export class InlineEditComponent implements OnInit {

  initialValue: String;
  @Input()
  value: String;

  @Output()
  save: EventEmitter<String> = new EventEmitter();

  @Output()
  change: EventEmitter<Object> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.initialValue = this.value;
  }

  public getValue(): String {
    return this.value;
  }
  
  public emitChangeSignal(): void {
    
    if( this.initialValue == this.value) return;

    this.initialValue = this.value;
    this.save.emit(this.value);

  } 

  public handleContentChange( event: any): void {
    
    if( this.initialValue == this.value) return;
    
    let message ={
      value: this.value
    }
        
    if( event.key == "S" && event.ctrlKey == true ){
      this.save.emit( this.value );
    } else{
      this.change.emit( message );
    }

  }
  
}

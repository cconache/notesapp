import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.css']
})

export class TextareaComponent implements OnInit {

  @Input()
  content: String;

  @Output()
  save: EventEmitter<String> = new EventEmitter();

  @Output()
  change: EventEmitter<Object> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }


  public getValue(): String {
    return this.content;
  }

  public handleContentChange( event: any): void {
    
    let message ={
      value: this.content
    }

    if( event.key == "S" && event.ctrlKey == true ){
      this.save.emit( this.content );
    } else{
      this.change.emit( message );
    }

  }

}

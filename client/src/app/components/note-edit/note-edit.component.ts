import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

import {Note} from '../../objects/Note';
import {User} from '../../objects/User';

import { Utils } from '../../utils/Utils';

import {NotesService} from '../../services/notes-service/notes.service';
import {OwnersService} from '../../services/owners-service/owners.service';
import {InvitationsService} from '../../services/invitations-service/invitations.service';
import { SocketService} from '../../services/socket-service/socket.service';
import { InlineEditComponent } from '../inline-edit/inline-edit.component';
import { TextareaComponent } from '../textarea/textarea.component';

@Component({
  selector: 'note-edit',
  templateUrl: './note-edit.component.html',
  styleUrls: ['./note-edit.component.css'],
  providers: [OwnersService]
})
export class NoteEditComponent implements OnInit {

  invite: boolean = false;
  noteOwners: User[];
  currentUserEmail: String;
  
  @ViewChild('titleComp') titleComp: InlineEditComponent;
  @ViewChild('contentComp') contentComp: TextareaComponent;
  
  private _note: Note;

  @Input() set note(value: Note){
    
    this._note = value;

    this.ownersService.getOwners( this.note.getId() )
    .subscribe( owners => {
      this.noteOwners = owners;
    });

  }

  get note(): Note {
    return this._note;
  }


  constructor(private notesService: NotesService, private ownersService: OwnersService,
  private invitationsService: InvitationsService,
  private socketService: SocketService ) {
   this.currentUserEmail = Utils.getCurrentUserEmail(); 
  }

  ngOnInit() {
  }

  public handleNoteChange( changeData: JSON ){
    
    if( !changeData["target"] || !changeData["value"]) return;    

    let message = {};
    let target = changeData["target"]

    message["value"] = changeData["value"]
    message["type"] = target+"-edit";
    message["target"] = this.note.getId();

    // console.log(message);
    
    this.socketService.sendMessageToRoom( message["target"], message);

  }

  public updateNote(): void {

    let newNote: JSON = <JSON>{}
    
    newNote["title"] = this.titleComp.getValue();
    newNote["content"] = this.contentComp.getValue();

    this.notesService.updateNote( this.note.getId(), newNote);

  }

  public requestInvitation( email: String ): void {
    if( email != "" ){
      this.invitationsService.sendInvitation( email, this.note.getId() );
    }

    this.invite = !this.invite;
  }

}

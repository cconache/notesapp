import { Component, OnInit} from '@angular/core';
import { UserIdentityService } from '../../services/user-identity/user-identity.service';
import { SocketService } from '../../services/socket-service/socket.service';
import {Router} from '@angular/router';

@Component({
  selector: 'login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})

export class LoginUserComponent implements OnInit {

  constructor( private userIdentityService: UserIdentityService,
              private socketService: SocketService,
              private router: Router) { }

  ngOnInit() {
  }

  public loginUser( email: String, password: String){
    
    this.userIdentityService.authenticate( email, password)
    .then( res => {

      this.router.navigate(['/dashboard']);
      // this.socketService.getInvitations();
      //redirect user to getNotes and profile 
    })
    .catch( _ => {});
  
  }
}

import { Component, OnInit } from '@angular/core';
import {InvitationsService} from '../../services/invitations-service/invitations.service';
import { Invitation } from '../../objects/Invitation';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'invitations',
  templateUrl: './invitations.component.html',
  styleUrls: ['./invitations.component.css']
})
export class InvitationsComponent implements OnInit {

  invitations: Invitation[] = [];

  constructor(private invitationsService: InvitationsService) {

    this.invitationsService.getReceivedInvitations()
    .subscribe( res =>{
      this.invitations = res;
    })
  }

  ngOnInit() {
  }
  
  public declineInvitation( invitation: Invitation): void {
    this.invitationsService.declineInvitation(invitation);
  }

  public getOwnership( invitation: Invitation ): void {
    this.invitationsService.acceptInvitation( invitation );
  }

}

import { Component, OnInit } from '@angular/core';
import { UserIdentityService } from '../../services/user-identity/user-identity.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  firstName: String = "";
  lastName: String = "";
  username: String = "";
  email: String = "";
  password: String = "";
  passwordConf: String = "";

  constructor(private userIdentityService: UserIdentityService, private router: Router) {}

  ngOnInit() {
  }

  public createAccount(): void {

    let userInfo: JSON = <JSON> {}
    
    userInfo["firstName"] = this.firstName;
    userInfo["lastName"] = this.lastName;
    userInfo["username"] = this.username;
    userInfo["email"] = this.email;
    userInfo["password"] = this.password;
    userInfo["passwordConf"] = this.passwordConf;

    this.userIdentityService.createAccount( userInfo )
    .then( _ => {
      this.router.navigate(['login']);
    })
    .catch( _ => {});

  }

}

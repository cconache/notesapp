import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Note } from '../../objects/Note';
import {NotesService} from '../../services/notes-service/notes.service';

@Component({
  selector: 'note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  @Input()
  note: Note;

  @Output()
  selected: EventEmitter<Note> = new EventEmitter();

  constructor( private notesService: NotesService ) { }

  ngOnInit() {
  }

  public deleteNote(): void {
    this.notesService.deleteNote( this.note.getId() )
  }

  public emitSelectedEvent(): void {
    this.selected.emit(this.note);
  }

}

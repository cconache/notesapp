export class Invitation {
    
    constructor( private uid: String, private host: String, private noteTitle: String, private noteId: String){
    }

    public getHost(): String {
        return this.host;
    }

    public getNoteTitle(): String {
        return this.noteTitle;
    }

    public getNoteId(): String{
        return this.noteId;
    }

    public getUid(): String {
        return this.uid;
    }
    
    
}
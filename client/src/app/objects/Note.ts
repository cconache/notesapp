export class Note{

    constructor( private id: String, 
                private title:String, 
                private content: String,
                private tags: String[],
                private owners: JSON[]){}
    
    public getId(): String {
        return this.id;
    }

    public getTitle(): String {
        return this.title;
    }

    public getContent(): String {
        return this.content;
    }

    public getTags(): String[] {
        return this.tags;
    }

    public getOwners(): JSON[] {
        return this.owners;
    }

    public setId( id: String ): void {
        this.id = id;
    }

    public setTitle( title: String ): void {
        this.title = title;
    }

    public setContent( content: String ): void {
        this.content = content;
    }

    public setTags( tags: String[] ): void {
        this.tags = tags;
    }

    public setOwners( owners: JSON[]): void {
        this.owners = owners;
    }

    public addTag( tag: String ): void {
        this.tags.push(tag);
    }

    public addOwner( owner: JSON): void {
        this.owners.push(owner);
    }
    
}
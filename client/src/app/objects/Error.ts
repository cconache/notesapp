import {Notification} from './Notification';

export class Error extends Notification {

    constructor( message: String ){
        super( message );
    }
    
}
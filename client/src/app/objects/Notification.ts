export class Notification {

    constructor( private message: String ){}

    public getMessage(){
        return this.message;
    }

}
import { Notification } from './Notification';

export class Success extends Notification {

    constructor( message: String ){
        super( message );
    }
    
}
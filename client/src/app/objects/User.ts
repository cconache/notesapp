export class User {

    constructor(
                private uid: String,
                private firstName: String,
                private lastName: String,
                private email: String,
                private username: String
    ){

    }

    public getUid(): String {
        return this.uid;
    }

    public getFirstName(): String {
        return this.firstName;
    }

    public getLastName(): String {
        return this.lastName;
    }

    public getEmail(): String{
        return this.email;
    }

    public getUsername(): String {
        return this.username;
    }

}
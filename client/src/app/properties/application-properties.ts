export class ApplicationProperties {
    
    private static readonly SOCKETS_ENDPOINT: String  = "http://localhost:8080/";

    private static endpoints: any = {
        'login': 'http://localhost:8080/users/authenticate/',
        'signUp': 'http://localhost:8080/users/new/',
        'getNotes': 'http://localhost:8080/notes/',
        'getNote': 'http://localhost:8080/notes/',
        'deleteNote': 'http://localhost:8080/notes/',
        'createNote': 'http://localhost:8080/notes/new',
        'updateNote': 'http://localhost:8080/notes/',
        'getInvitations': 'http://localhost:8080/invitations/',
        'declineInvitation': 'http://localhost:8080/invitations/',
        'getOwners': 'http://localhost:8080/owners/',
        'sendInvitation': 'http://localhost:8080/invitations/',
        'acceptInvitation': 'http://localhost:8080/owners/',
        'getProfile': 'http://localhost:8080/users/'
    }

    public static getEndpointURL( url ): string {
        return this.endpoints[url];
    }

    public static getSocketsEndpoint(): String {
        return this.SOCKETS_ENDPOINT;
    }

}
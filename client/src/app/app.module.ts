import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './routing/app-routing.module';

import { AppComponent } from './app.component';
import { LoginUserComponent } from './components/login-user/login-user.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { NotesListComponent } from './components/notes-list/notes-list.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NotificationComponent } from './components/notification/notification.component';
import { NoteComponent } from './components/note/note.component';
import { NoteEditComponent } from './components/note-edit/note-edit.component';
import { InlineEditComponent } from './components/inline-edit/inline-edit.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { InvitationsComponent } from './components/invitations/invitations.component';

import { HttpModule } from '@angular/http';
import { UserIdentityService } from './services/user-identity/user-identity.service';
import { NotificationService } from './services/notification-service/notification.service';
import { NotesService } from './services/notes-service/notes.service';
import { SocketService } from './services/socket-service/socket.service';
import { InvitationsService } from './services/invitations-service/invitations.service';

import { Utils } from './utils/Utils';
import { ApplicationProperties } from './properties/application-properties';

import {AuthGuard} from './routing/guards/auth/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    LoginUserComponent,
    CreateUserComponent,
    NotesListComponent,
    DashboardComponent,
    NotificationComponent,
    NoteComponent,
    NoteEditComponent,
    InlineEditComponent,
    TextareaComponent,
    InvitationsComponent
  ],
  imports: [
    FormsModule,
    HttpModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    NotificationService,
    UserIdentityService,
    NotesService,
    SocketService,
    InvitationsService,
    AuthGuard,
    Utils,
    ApplicationProperties
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

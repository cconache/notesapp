import { Headers } from '@angular/http';

export class Utils {

    private static userEmail: String;

    public static getJsonResponse( response ){
        let res = response.json(); 
        res.status = response.status;
        return res;
    }

    public static getAuthorizationHeader(){
        
        let headers = new Headers();
        let token = localStorage.getItem("token") || "";
        headers.append("x-auth-token", token);

        return headers;
    }

    public static generateUuid(): String{
        
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    public static setCurrentUserEmail( email: String ){
        this.userEmail = email;
    }

    public static getCurrentUserEmail(): String {
        return this.userEmail
    }

}
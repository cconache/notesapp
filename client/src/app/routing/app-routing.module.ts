import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth/auth.guard';
import { LoginUserComponent } from '../components/login-user/login-user.component';
import { CreateUserComponent } from '../components/create-user/create-user.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';

const routes: Routes = [

  { path: 'login', component: LoginUserComponent },
  { path: 'signup', component: CreateUserComponent },
  { path: 'dashboard', 
  canActivate: [AuthGuard],
  component: DashboardComponent }
  
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})



export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SocketService } from '../../../services/socket-service/socket.service';
import { UserIdentityService } from '../../../services/user-identity/user-identity.service';
import { NotificationService } from '../../../services/notification-service/notification.service';
import { Error } from '../../../objects/Error';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor( private socketService: SocketService,
              private userIdentityService: UserIdentityService,
              private router: Router  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    return this.userIdentityService.userLoggedIn().then( token => {

      if( !this.socketService.getSocket() ){
        
        return this.socketService.connectToServer( token ).then( _ =>{
          console.log("connected socket");
          return true;
        })
        .catch( _ => {

          this.router.navigate(['/login']);
          return false;
       
        });
        
      }

    })
    .catch( _ => {

      this.router.navigate(['/login']);
      return false;
    
    })
  }
}

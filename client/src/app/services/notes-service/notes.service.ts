import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Notification } from '../../objects/Notification';
import { Success } from '../../objects/Success';
import { Error } from '../../objects/Error';
import { Note } from '../../objects/Note';

import { ApplicationProperties } from '../../properties/application-properties';
import { Utils } from '../../utils/Utils';
import { NotificationService } from '../notification-service/notification.service';
import { Observable } from 'rxjs/Observable';
import { SocketService } from '../socket-service/socket.service';

@Injectable()
export class NotesService {

  private notesObservable: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>([]);
  private notes: Note[];

  constructor( private http: Http, 
              private notificationService: NotificationService,
              private socketService: SocketService ) {
  
    this.loadNotes();
    
    this.socketService.getNotesUpdates()
    .subscribe( (message) => {
      
      let noteId = message["target"];
      
      switch( message["type"] ){

        case "note-update":
          this.loadNote(noteId);
          break;
        
        case "note-delete":
          this.locallyDeleteNote(noteId);
          break;

        case "title-edit":{ 

            let res = this.notes.filter( note => note.getId() == noteId);
            if( res.length > 0 && res[0] ){
              res[0].setTitle( message["value"] );
            }
            break;
          }
        
        case "content-edit":{
         
          let res = this.notes.filter( note => note.getId() == noteId);
          if( res.length > 0 && res[0] ){
            res[0].setContent( message["value"] );
          }

          break;
        }

      }
      

    })


  }

  public getLoadedNotes(): Observable<Note[]> {                                                                     
    return this.notesObservable.asObservable();
  }

  public loadNotes(): void {

    this.getNotes()
    .then( response => {
      
      if( response.status == 200 ){
        this.storeNotes( response.notes );
      }else{
        let responseBody = JSON.parse(response._body);
        this.notificationService.addNotification( new Error(responseBody.message) );
      }

    });

  }

  public loadNote( noteId: String ): void {
    
    this.getNote( noteId )
    .then( response => {
      
      if( response.status == 200 ){
        this.locallyUpdateNote( noteId, response.note );
      }

    });

  }
    
  private getNote( noteId: String ): Promise<any> {

    return this.http.get( ApplicationProperties.getEndpointURL('getNote')+noteId, { headers: Utils.getAuthorizationHeader()})
    .toPromise()
    .then( Utils.getJsonResponse )
    .catch( err => {return err;});

  }

      
  private storeNotes( notes: JSON[] ){
    this.notes = [];
    for( let note of notes) this.addToStorage(note);
    this.notesObservable.next( this.notes );

  }

  private addToStorage( note: JSON ): void {

    let obj = new Note( note["_id"], note["title"], note["content"], note["tags"], note["owners"]);
    this.notes.push(obj);
  
  }

  private getNotes(): Promise<any> {

    return this.http.get( ApplicationProperties.getEndpointURL('getNotes'), {headers: Utils.getAuthorizationHeader() })
    .toPromise()
    .then( response => {
      return Utils.getJsonResponse( response );
    })
    .catch( err => { return err});

  }

  private requestNoteDeletion( noteId: String ): Promise<any> {

    return this.http.delete( ApplicationProperties.getEndpointURL('deleteNote') + noteId, {headers: Utils.getAuthorizationHeader() })
           .toPromise()
           .then( Utils.getJsonResponse )
           .catch( err => { return err } );

  }

  private requestNoteCreation( noteInfo: JSON ): Promise<any> {

    return this.http.post( ApplicationProperties.getEndpointURL('createNote'), noteInfo , {headers: Utils.getAuthorizationHeader() })
    .toPromise()
    .then( Utils.getJsonResponse )
    .catch( err => { return err} );

  }

  private requestNoteUpdate( noteId: String, noteData: JSON ): Promise<any> {

    return this.http.put( ApplicationProperties.getEndpointURL('updateNote')+noteId, noteData, {headers: Utils.getAuthorizationHeader()})
    .toPromise()
    .then( Utils.getJsonResponse )
    .catch( err => { return err} );

  }

  private locallyUpdateNote( noteId: String, noteData: JSON): void {

    for( let i in this.notes){
      
      if( this.notes[i].getId() == noteId ){

        if( noteData["title"] ) {
          this.notes[i].setTitle( noteData["title"] );
        }

        if( noteData["tags"] ) {
          this.notes[i].setTags( noteData["tags"] );
        }

        if( noteData["content"] ){
          this.notes[i].setContent( noteData["content"]);
        }

      }
      
    }
    
    this.notesObservable.next(this.notes);

  }

  public updateNote( noteId: String, newNote: JSON ): Promise<any> {
    
    return new Promise( (resolve, reject) => {

      this.requestNoteUpdate(noteId, newNote)
      .then( response => {
        
        if( response.status == 200 ){

          this.locallyUpdateNote( noteId, newNote );
          resolve();
        
        }else{
        
          let responseBody = JSON.parse(response._body);
          this.notificationService.addNotification( new Error(responseBody.message) );
          reject();
        
        }
  
      });
    });
    
  }


  public deleteNote( noteId: String ): void {

    this.requestNoteDeletion( noteId )
    .then( response => {
      
      if( response.status == 200 ){
        this.locallyDeleteNote( noteId );
      } else {

        let responseBody = JSON.parse(response._body);
        this.notificationService.addNotification( new Error(responseBody.message) );
      
      }

    })
    
  }

  public createNote( noteInfo: JSON ): void {
    
    this.requestNoteCreation( noteInfo ).then( response => {

      if( response.status == 200 ){

        this.addToStorage( response );
        this.notesObservable.next( this.notes);
        
      }else{
        this.notificationService.addNotification( new Error("Note could not be created.") );
      }

    })
  }

  private locallyDeleteNote( noteId: String ): void {
    
    this.notes = this.notes.filter( note => note.getId() !== noteId );
    this.notesObservable.next( this.notes );

  }

}

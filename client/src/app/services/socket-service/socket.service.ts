import { Injectable } from '@angular/core';
import * as io from 'socket.io/node_modules/socket.io-client';
import { ApplicationProperties } from '../../properties/application-properties';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class SocketService {

  private socket;

  constructor() {}

  public connectToServer( token: String ): Promise<any> {

    var success = true;
    let options = {
      query: {
        token: token
      }
    }

    this.socket = io( ApplicationProperties.getSocketsEndpoint(), options);

    return new Promise( (resolve, reject) => {

      this.socket.on('unauthorized', _ => {
        reject()
      });      
      this.socket.on('authorized', _ => {
        resolve();
      });

    });
  }

  public getInvitations(): Observable<JSON>{

    return new Observable( observer => {
  
      this.socket.on("invitation request", (data) => {
        observer.next( <JSON>data );    
      });

    })    
  }

  public getNotesUpdates(): Observable<JSON>{

    return new Observable( observer => {

      this.socket.on("note-update", (data) => {
        observer.next( <JSON>data );
      })

      this.socket.on("note-delete", (data) => {
        observer.next(<JSON>data);
      })

      this.socket.on('title-edit', (data) => {
        observer.next(<JSON>data);
      })

      this.socket.on('content-edit', (data) => {
        observer.next(<JSON> data);
      });

    });

  }

  public getOwnersUpdates(): Observable<JSON> {

    return new Observable( observer => {

      this.socket.on("owner-new", (data) => {
        observer.next( <JSON>data );
      });

    })
    
  }

  public sendMessageToRoom( roomName, message ){
    message.room = roomName
    this.socket.emit('room-message', message);
  }

  public getSocket(): io {
    return this.socket;
  }

}

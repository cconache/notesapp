import { Injectable } from '@angular/core';
import { User } from '../../objects/User';
import {Http} from '@angular/http';
import {BehaviorSubject} from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { ApplicationProperties } from '../../properties/application-properties';
import { Utils } from '../../utils/Utils';
import { SocketService } from '../socket-service/socket.service';

@Injectable()
export class OwnersService {

  private ownersObservable: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  private owners: User[] = [];
  private currentNoteId: String;

  constructor( private http: Http, private socketService: SocketService) {
    this.owners = [];

    this.socketService.getOwnersUpdates()
    .subscribe( message => {
      
      let noteId = message["target"];

      switch( message["type"]){
        
        case "owner-new":
          if( noteId == this.currentNoteId ){
            this.loadOwners(noteId);
          }
          break;
          
      }
    })
  }

  private loadOwners( noteId: String ): void {

    this.owners = [];
    
    this.requestOwners( noteId )
    .then( response => {
      
      if( response.status == 200 ){
        for( let owner of response.owners ){
          this.storeOwner(owner);
        }
        this.ownersObservable.next(this.owners);
      }
      
    });

  }

  getOwners( noteId: String ): Observable<User[]>{
    
    this.currentNoteId = noteId;
    this.loadOwners( noteId );
    return this.ownersObservable.asObservable();

  }

  private storeOwner( user: JSON ){

    if( user["_id"] && user["firstName"] && user["lastName"] && user["email"] && user["username"]){
      let owner = new User(user["_id"], user["firstName"], user["lastName"], user["email"], user["username"]);
      this.owners.push(owner);
    }

  }

  private requestOwners( noteId: String){

    return this.http.get( ApplicationProperties.getEndpointURL('getOwners')+noteId, { headers: Utils.getAuthorizationHeader() })
    .toPromise()
    .then( Utils.getJsonResponse )
    .catch( err => {return err});

  }

}

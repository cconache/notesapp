import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import { Notification } from '../../objects/Notification';

@Injectable()
export class NotificationService {

  private activeNotification: Subject<Notification> = new Subject();

  public getActiveNotification(): Observable<Notification> {
    return this.activeNotification;
  }

  public addNotification( notification: Notification ){
    this.activeNotification.next( notification );
  }

  
}

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {NotificationService} from '../notification-service/notification.service';
import {SocketService} from '../../services/socket-service/socket.service';
import { Notification } from '../../objects/Notification';
import { Success } from '../../objects/Success';
import { Error } from '../../objects/Error';

import { ApplicationProperties } from '../../properties/application-properties';
import { Utils } from '../../utils/Utils';

@Injectable()
export class UserIdentityService {

  constructor( private http: Http, 
               private notificationService: NotificationService, 
               private socketService: SocketService ) { }

  public userLoggedIn(): Promise<any> {

    return new Promise( (resolve, reject) => {
      
      this.requestUserProfile().then( response => {

          if( response.status == 401 ) reject()
          else resolve( localStorage.getItem("token") );
        
        }
      ).catch( _ => reject() );  
    });

  }

  public authenticate( email: String, password: String ): Promise<any> {
    
    return new Promise( (resolve, reject) => {
      this.requestAuthentication( email, password).then( response => {
                
        if( response.status == 200 ){
          localStorage.setItem("token", response.token);
          this.socketService.connectToServer( response.token )
          .then( _ => {
            Utils.setCurrentUserEmail(email);
            this.notificationService.addNotification( new Success("User successfully logged in.") );   
          })
          .catch( _ => {
            localStorage.removeItem("token");
            this.notificationService.addNotification( new Error("Couldn't complete user login. Please try again.") );        
          })

          resolve();
        
        } 

        if( response.status != 200 ){
          // let responseBody = JSON.parse(response._body);      
          this.notificationService.addNotification( new Error("Couldn't complete user login. Please try again.") );
          reject();
        }

      })  
    }); 
  }
  
  public createAccount( userInfo: JSON ): Promise<any> {

    return new Promise( (resolve, reject) => {
      this.requestAccountCreation( userInfo ).then( response => {
        
        if( response.status == 200 ){
          this.notificationService.addNotification( new Success("Accont successfully created. Please login!") );
          resolve();
        } else{
          let responseBody = JSON.parse(response._body);        
          this.notificationService.addNotification( new Error(responseBody.message) );
          reject();
        }
      });
    });
  }

  private requestUserProfile(): Promise<any> {
    
    return this.http.get( ApplicationProperties.getEndpointURL('getProfile'), { headers: Utils.getAuthorizationHeader() })
    .toPromise()
    .then( Utils.getJsonResponse)
    .catch( err => {return err});

  }
  private requestAuthentication( email: String, password: String): Promise<any> {

    const url = ApplicationProperties.getEndpointURL('login');
    const body = {
      email: email,
      password: password
    }

    return this.http.post(url, body).toPromise()
           .then( response => {
             return Utils.getJsonResponse( response ); 
             } )
           .catch( err => {return err});
  };

  private requestAccountCreation( userInfo: JSON ): Promise<any> {

    const url = ApplicationProperties.getEndpointURL('signUp');
    
    return this.http.post(url, userInfo).toPromise()
    .then( response => { return Utils.getJsonResponse( response ) })
    .catch( err => { return err; } );

  } 

}

import { TestBed, inject } from '@angular/core/testing';

import { UserIdentityService } from './user-identity.service';

describe('UserIdentityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserIdentityService]
    });
  });

  it('should be created', inject([UserIdentityService], (service: UserIdentityService) => {
    expect(service).toBeTruthy();
  }));
});

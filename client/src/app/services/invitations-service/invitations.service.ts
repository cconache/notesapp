import { Injectable } from '@angular/core';
import { Invitation } from '../../objects/Invitation';
import { BehaviorSubject, Observable } from 'rxjs';

import {SocketService} from '../socket-service/socket.service';
import {NotesService} from '../notes-service/notes.service';

import {NotificationService} from '../notification-service/notification.service';
import { Notification } from '../../objects/Notification';
import { Success } from '../../objects/Success';
import { Error } from '../../objects/Error';

import { Http } from '@angular/http';
import { ApplicationProperties } from '../../properties/application-properties';
import { Utils } from '../../utils/Utils';

@Injectable()
export class InvitationsService {

  private invitationsObservable: BehaviorSubject<Invitation[]> = new BehaviorSubject<Invitation[]>([]);
  private invitations: Invitation[] = [];

  constructor( private http: Http, private socketService: SocketService,
  private notificationService: NotificationService,
  private notesService: NotesService) { 

    this.requestInvitations().then( response => {
      if( response.status == 200 ){
        for( let invitation of response["invitations"]){
          this.storeInvitation(invitation);
        }
      }

      this.invitationsObservable.next(this.invitations);
      this.startInvitationListener();

      }
    ).catch( _ => {
      this.invitationsObservable.next( this.invitations );
    })

  }

  public getReceivedInvitations(): Observable<Invitation[]> {
    return this.invitationsObservable.asObservable();
  }

  public sendInvitation( email: String, noteId: String ): void {

    let data: JSON = <JSON>{};
    data["email"] = email;
    data["noteId"] = noteId;
    
    this.requestInvitationSend( data )
    .then( response => {

      if( response.status == 200 ){
        this.notificationService.addNotification( new Success("Invitation successfully sent to "+email));
      } else{
        let res = JSON.parse(response._body);
        this.notificationService.addNotification( new Error(res["message"]) );
      }

    });

  }

  public declineInvitation( invitation: Invitation ): void {
    
    this.requestDecline( invitation ).then( response => {

      if( response.status  == 200 ) this.hideInvitation( invitation );

    })
  }

  public acceptInvitation( invitation: Invitation ): void {

    this.hideInvitation( invitation );

    this.requestAccept( invitation ).then( response => {

        if (response.status != 200 ) {
        let res = JSON.parse( response._body );
        this.notificationService.addNotification( new Error(res["message"]) );
      } else {
        this.notesService.loadNotes();
      }

    });
  }

  private requestAccept( invitation ): Promise<any> {

    return this.http.post( ApplicationProperties.getEndpointURL('acceptInvitation'),{ invitationId: invitation.getUid() },
    {headers: Utils.getAuthorizationHeader()})
    .toPromise()
    .then( Utils.getJsonResponse )
    .catch( err => { return err; });

  }

  private hideInvitation( invitation: Invitation ){
    
    this.invitations = this.invitations.filter( inv => {
      return invitation.getUid() !== inv.getUid();
    });

    this.invitationsObservable.next(this.invitations);

  }

  private requestInvitationSend( reqBody: JSON ): Promise<any> {

    return this.http.post( ApplicationProperties.getEndpointURL('sendInvitation'), reqBody, { headers: Utils.getAuthorizationHeader()})
    .toPromise()
    .then( Utils.getJsonResponse)
    .catch( err => {return err});

  } 


  private storeInvitation( invitationData: JSON ): void {
    if( invitationData["uid"] && invitationData["host"] && invitationData["note"] ){

        let noteId = invitationData["note"]["noteId"] || invitationData["note"]["_id"];
        let title = invitationData["note"]["title"];

        if( noteId && title ){
        
        let invitation = new Invitation( invitationData["uid"], invitationData["host"], title, noteId);
        this.invitations.push(invitation);
        
      }
    }
  
  }

  private startInvitationListener() {

    this.socketService.getInvitations()
    .subscribe( invitationData => {
    
          this.storeInvitation( invitationData );
          this.invitationsObservable.next( this.invitations );
    })
  }

  private requestInvitations() {
    return this.http.get( ApplicationProperties.getEndpointURL('getInvitations'), {headers: Utils.getAuthorizationHeader() })
    .toPromise()
    .then( Utils.getJsonResponse )
    .catch( err => { return err});
  }

  private requestDecline( invitation: Invitation ){

    return this.http.delete( ApplicationProperties.getEndpointURL('declineInvitation')+invitation.getUid(),
    {headers: Utils.getAuthorizationHeader()})
    .toPromise()
    .then( Utils.getJsonResponse )
    .catch( err => { return err; });

  }
}
